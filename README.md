# Api GeoIP

*REST Api* para obter a geolocalização de um determinado **IPv4**.

Para consultar documentação da api basta ir no caminho `/docs` ou `/redoc`.

<p align="center">
  <img src="./assets/docs.png" />
</p>

## Basic Usage

```bash
curl -X GET http://127.0.0.1:8000/v1/187.19.212.134
```
```json
{
    "status": "success",
    "continent": "South America",
    "continentCode": "SA",
    "country": "Brazil",
    "countryCode": "BR",
    "region": "PB",
    "regionName": "Paraíba",
    "city": "João Pessoa",
    "district": "",
    "zip": "",
    "lat": -7.114,
    "lon": -34.8605,
    "timezone": "America/Fortaleza",
    "isp": "Brisanet Servicos De Telecomunicacoes Ltda",
    "org": "Brisanet Servicos De Telecomunicacoes Ltda",
    "as": "AS28126 BRISANET SERVICOS DE TELECOMUNICACOES LTDA",
    "reverse": "187-19-212-134-tmp.static.brisanet.net.br",
    "hosting": false,
    "query": "187.19.212.134"
}
```

## Install

### Python

Para rodar em Python basta instalar as depedências e em seguida utilizar o uvicorn para executar a API.

```bash
python -m pip install -r requirements.txt
uvicorn app:app
```

### Docker

```bash
docker build -t geoip:latest .
docker run --rm -p 8000:8000 geoip:latest
```